import {index } from '../../../types';


export default (state, action) => {
switch(action.type) {
    case index.FORMULARIO_USUARIOS:
        return {
            //aca toma una copia del state con todo lo que tenga y lo cambia a true el formulario
            ...state,
            formulario : true
        }
    case index.OBTENER_USUARIOS:
        return {
            ...state,
            usuarios: action.payload
        }
    case index.AGREGAR_USUARIO:
        return {
            //se copia el state actual
            ...state,
            //se toma el state con los proyectos y se agrega el nuevo con action.payload
            usuarios: [...state.usuarios, action.payload],
            //despues que se agrega el proyecto quedara como false para que se reinicie
            formulario: false,
            //una vez que se valida y se agrega la nueva tarea, reseteamos el errorformulario 
            //para que deje de aparecer en pantalla
            errorformulario: false
            
        }
    case index.VALIDAR_FORMULARIO:
        return {
            ...state,
            errorformulario: true
        }
    case index.USUARIO_ACTUAL:
        return {
            ...state,
            //vamos a poner un filtro, 
            //hace una iteración con cada uno de ellos comparandolos, extrae el que se elige como proyecto actual
            usuario: state.usuarios.filter(usuario => usuario._id === action.payload )
        }
    case index.ELIMINAR_USUARIO:
        return {
            //aca recorre los proyectos pero deja afuera el que queremos eliminar !==..
            usuarios: state.usuarios.filter(usuario => usuario._id !== 
                action.payload ),
                usuario: null
        }
    case index.ACTUALIZAR_USUARIO:
        return {
            ...state,
            usuariosproyecto: state.usuariosproyecto.map(usuarios => usuarios._id === 
                action.payload.id ? action.payload: usuarios) 
        }
    case index.PROYECTO_ERROR:
        return {
            ...state,
            mensaje: action.payload
        }      
    default:
        return state;
}
}