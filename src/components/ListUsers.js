import React, { useState, useEffect, Fragment } from 'react';
import { Image, List, Table, Header, Icon } from 'semantic-ui-react'

import clienteAxios  from '../config/axios';
import { CSSTransition, TransitionGroup } from 'react-transition-group'


//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
    const [users, setUsers] = useState([])


    useEffect (() => {
        console.log('useEffect')
        obtenerDatos()

    }, [])
  

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
    }

    return (
        <Fragment>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Cargo</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Icon color='green' name='edit' />
                <Icon color='red' name='user delete' />
                

            </Table.Row>
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


export default ListUsers;

 
