import React from 'react';
//import { Link } from 'react-router-dom';
//import { Grid, Image } from 'semantic-ui-react';
//import { Router, Route, Link } from 'react-router';
//import Header from './Header';
import { ThemeProvider } from '@material-ui/core/styles';
import theme from '../../themeConfig';
import Contenedor from '../layout/Contenedor';

/*
import Navbar from '../layout/Navbar';
import Listas from '../layout/Listas';
import Oculto from '../layout/Oculto';
*/




const Landingpage = () => {
    return (

        <ThemeProvider theme={theme}>
                    
                <Contenedor />

        </ThemeProvider>

        
    )
}

export default Landingpage
