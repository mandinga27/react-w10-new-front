import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import MenuItem from '@material-ui/core/MenuItem';
import Grid from '@material-ui/core/Grid';


const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    backgroundColor: '#DCDCDC',
    height: '100vh',
    backgroundSize: 'cover',
    backgroundPosition: 'center',
    alignItems: 'center',
    fontFamily: 'Roboto',
    
  },
  textField: {
    marginLeft: theme.spacing(1),
    marginRight: theme.spacing(1),
    width: '25ch',
    backgroundColor: '#FFFFFF',
    //fontSize: 88,
  },
  text: {
      fontSize: 58,
  },
  label:{
      fontSize: 30.
  },
  background: {
      backgroundColor: 'white',
      display: 'flexWrap',
  },
}));


const area = [
    {
      value: 'Bodega',
      label: 'Bodega'
    },
    {
      value: 'exterior',
      label: 'Exterior'
    },
    {
      value: 'interior',
      label: 'Interior'
    },
  ];
  /*
  const lugar = [
    {
      value: 'Patio',
      label: 'Patio'
    },
    {
      value: 'exterior',
      label: 'Exterior'
    },
    {
      value: 'cargo',
      label: 'Jefe de SSOMA'
    },
  ];
  */

  const tema = [
    {
      value: 'Seguridad Ocupacional',
      label: 'Seguridad Ocupacional'
    },
    {
      value: 'ergonomía',
      label: 'Ergonomía'
    },

  ];

const Charla = () => {
    const classes = useStyles();

    return (
        <Container component="main" maxWidth="md"
            className={classes.background}
        >
            <Typography component="h1" variant="h2"
                
            >
                Registro de Charlas
            </Typography>
            <form
                className={classes.background}
            >                
                <div className="row">
                        <div>

                        <Grid container spacing={5}>
                            <Grid item xs={12} sm={6}>
                            <TextField 
                                htmlFor="area"
                                variant="outlined"
                                required
                                type="text"
                                name="area"
                                placeholder="Ingresa el area de trabajo"
                                fullWidth
                                select
                                label="Área"
                            
                            >
                            {area.map((option) => (
                                <MenuItem key={option.value} value={option.value}>
                                {option.label}
                                </MenuItem>
                            ))}
                             </TextField>
                        </Grid>
                        <div>
                            
                        </div>
                        <Grid item xs={12} sm={7}>
                            <TextField 
                                htmlFor="tema"
                                variant="outlined"
                                required
                                type="text"
                                name="tema"
                                placeholder="Ingresa el tema de la charla"
                                fullWidth
                                select
                                label="Tema"                         
                            >
                            {tema.map((option) => (
                                <MenuItem key={option.value} value={option.value}>
                                {option.label}
                                </MenuItem>
                            ))}
                             </TextField>
                        </Grid>
                        </Grid>    
                        </div>

                        
                        
               
                </div>          
            </form> 
            
        </Container>
        
  );
}

export default Charla;