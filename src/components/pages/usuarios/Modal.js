import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Modal from '@material-ui/core/Modal';
import Backdrop from '@material-ui/core/Backdrop';
import Fade from '@material-ui/core/Fade';
import Button from '@material-ui/core/Button';
import Axios from 'axios'
import TextField from '@material-ui/core/TextField';
import clienteAxios  from '../../../config/axios';
// import { baseurl } from '../../store/middleware'
// import scheduleActions from '../../store/actions/scheduleActions'
// import ScheduleMiddleware from '../../store/middleware/ScheduleMiddleware';

const useStyles = makeStyles((theme) => ({
  modal: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
  },
  paper: {
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));

export default function TransitionsModal(props) {
  const classes = useStyles();
  const open = props.openModal;
  const [nombre, setNombre] = React.useState(props.user.nombre)
  const [apellido, setApellido] = React.useState(props.user.apellido)
  const [email, setEmail] = React.useState(props.user.email)
  const [cargo, setCargo] = React.useState(props.user.cargo)
  // let dispatch = useDispatch();
  const handleClose = () => {
    props.closeModalMatch();
  };
  const handleOpen = () => {
    props.openModalMatch();
  };
  const handleChangeNombre=(e)=>{
    e.preventDefault();
    setNombre(e.target.value) 
  }
  const handleChangeApellido=(e)=>{
    e.preventDefault();
    setApellido(e.target.value) 
  }
  const handleChangeCargo=(e)=>{
    e.preventDefault();
    setCargo(e.target.value) 
  }
  const handleChangeEmail=(e)=>{
    e.preventDefault();
    setEmail(e.target.value) 
  }
  

    const data = {
      id: props.user._id,
      usuario: props.user._id,
      nombre: nombre,
      apellido: apellido,
      email: email,
      cargo: cargo
    }
    
    console.log(data)
    const actualizarUser = async data => {
      console.log(data)
      try{
        const result = await clienteAxios.put(`/api/usuarios/${props.user._id}`,data)
        return alert("Usuario actualizado con exito"+result)
      }catch(error){
        return console.error(error)
      } 
    }

    // dispatch(ScheduleMiddleware.takeGame(data))
    // const user = localStorage.getItem('user')
    // const token = localStorage.getItem('token')
    // Axios.post(`${ baseurl }/schedule/remove-schedule`,{ _id, user },{ headers: { Authorization: "Bearer " + token } 
    // }).then(res =>{
    //   if ( res.data.success ){
    //     alert("se agendo el partido, puedes contactar a tu rival desde tu perfil!")
    //   }
    //   dispatch(ScheduleMiddleware.getAllSchedule());
    // }
    // ).catch(err => console.log( err ))

  // const GetHorarios = () =>{
  //   const horarios = props.match.horario.map((horario)=>
  //     <li key={props.match._id}>hora disponible: {horario.label}</li>)
  //   return horarios
  // }

  return (
    <div>
      <Modal
        aria-labelledby="transition-modal-title"
        aria-describedby="transition-modal-description"
        className={classes.modal}
        open={open}
        onClose={handleClose}
        closeAfterTransition
        BackdropComponent={Backdrop}
        BackdropProps={{
          timeout: 500,
        }}
      >
        <Fade in={open}>
          <div className={classes.paper}>
            <h2 id="transition-modal-title">Confirmación</h2>

            <TextField
          id="standard-helperText"
          label="Nombre"
          defaultValue={nombre}
          onChange={handleChangeNombre}
          helperText="Some important text"
        />
            <TextField
          id="standard-helperText"
          label="Apellido"
          defaultValue={apellido}
          onChange={handleChangeApellido}
          helperText="Some important text"
        />
            <TextField
          id="standard-helperText"
          label="Cargo"
          defaultValue={cargo}
          onChange={handleChangeCargo}
          helperText="Some important text"
        />
            <TextField
          id="standard-helperText"
          label="Email"
          defaultValue={email}
          onChange={handleChangeEmail}
          helperText="Some important text"
        />
            {/* <p id="transition-modal-description">Vas a jugar con {props.match.user[0].nombre}</p>
            <p id="transition-modal-description">El día {props.match.fecha}</p> */}
            {/* <GetHorarios /> */}
            <>
            <Button onClick={handleClose}>Salir</Button>
            <Button type="submit" onClick={()=>actualizarUser(data)}>Confirmar</Button>
            </>
          </div>
        </Fade>
      </Modal>
    </div>
  );
}