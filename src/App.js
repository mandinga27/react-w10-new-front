import React from 'react';

//importamos BrowserRouter para crear nuestras rutas
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './components/auth/Login';


import Registro from './components/auth/Registro';
import Proyectos from './components/pages/proyectos/Proyectos';

import Charla from './components/pages/Charla';

import ListUsers from './components/pages/usuarios/ListUsuarios';



import ProyectoState from './components/hooks/proyectos/proyectoState';
import TareaState from './components/hooks/tareas/tareaState';
import AlertaState from './components/hooks/alertas/alertaState';

import AuthState from  './components/hooks/autentificacion/authState';
import tokenAuth from './config/tokenAuth';
import RutaPrivada from './components/rutas/RutaPrivada';
import UsuarioState from './components/hooks/usuarios/usuarioState';
import Usuarios from './components/pages/usuarios/Usuarios';


//Revisar su tenemos un token
const token = localStorage.getItem('token');
if(token) {
  tokenAuth(token);
}
function App() {
  
  console.log('http://localhost:4000');
  
  return (
    //todo lo que este destro del Switch serán nuestras diferentes paginas
    //todo lo que este por fuera de Switch es lo que se vera en todas las paginas
    //agregamos nuestro ProyectoState para el Provider
    //se poner al inicio para que este disponible para todos los componentes y props  la app
    //similar a redux pero en context
      
      
      //<Navbar />
      <ProyectoState>
        <UsuarioState>
        <TareaState>
          <AlertaState>
            <AuthState>          
              <Router>              
                <Switch>                 
                  <Route exact path="/" component={Login} />
                  <Route exact path="/registro" component={Registro} />
                  <Route exact path="/usuarioseditar/:id" component={Usuarios} />

                  <Route exact path="/listusers" component={ListUsers} />

                  <Route exact path="/charla" component={Charla} />




                  <RutaPrivada exact path="/proyectos" component={Proyectos} />
                </Switch>
              </Router>
            </AuthState>  
          </AlertaState>     
      </TareaState>
      </UsuarioState>
    </ProyectoState>
       
  );
}
  


export default App;
