import React from 'react';
import { mount } from 'enzyme';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import Login from '../../../components/auth/Login';

const middlewares = [ thunk ];
const mockStore = configureStore( middlewares );

const initState = {};
const store = mockStore( initState );
store.dispatch = jest.fn();

const wrapper = mount(
    <Provider store={ store } >
        <Login />
    </Provider>
)

describe('Pruebas en <Login />', () => {

    test('debe mostartse correctamente', () => {
        
        expect( wrapper).toMatchSnapshot();
    })
    
})