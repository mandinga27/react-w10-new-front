import React from 'react';
import { mount } from 'enzyme';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import Login from '../../../components/auth/Login';
import ProyectoState from '../../../context/proyectos/proyectoState';
import TareaState from '../../../context/tareas/tareaState';
import AlertaState from '../../../context/alertas/alertaState';
import AlertaContext from '../../../context/alertas/alertaContext';
import AuthContext from '../../../context/autentificacion/authContext';
import AuthState from '../../../context/autentificacion/authState'; 

const middlewares = [ thunk ];
const mockStore = configureStore( middlewares );
 
const initState = {};
const store = mockStore( initState );
store.dispatch = jest.fn(); 

const wrapper = mount(
    <Provider store={ store } >
        <AlertaState >
            <AuthState>
                <Login />
            </AuthState>
        </AlertaState>       
    </Provider>
)

describe('Pruebas en <Login />', () => {

    test('debe mostartse correctamente', () => {
        
        expect( wrapper).toMatchSnapshot();
    })
    
})