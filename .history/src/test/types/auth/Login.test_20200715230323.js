import React from 'react';
import { mount, shallow } from 'enzyme';
import sinon from 'sinon';
import { Provider } from 'react-redux';

import configureStore from 'redux-mock-store';
import thunk from 'redux-thunk';

import Login from '../../../components/auth/Login';
import ProyectoState from '../../../context/proyectos/proyectoState';
import TareaState from '../../../context/tareas/tareaState';
import AlertaState from '../../../context/alertas/alertaState';
import AlertaContext from '../../../context/alertas/alertaContext';
import AuthContext from '../../../context/autentificacion/authContext';
import AuthState  from '../../../context/autentificacion/authState';
import UsuarioState from '../../../context/usuarios/usuarioState'; 
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import { Item } from 'semantic-ui-react';

//import {LOGIN_EXITOSO} from '../../../context/autentificacion/authReducer';
//import { iniciarSesion } from '../../../context/autentificacion/authContext';

/*
jest.mock('../../../context/autentificacion/authState', () => ({
    iniciarSesion: jest.fn()
}))
*/
const middlewares = [ thunk ];
const mockStore = configureStore( middlewares );
 
const initState = {};
const store = mockStore( initState );
store.dispatch = jest.fn(); 

const wrapper = mount( 
    <Provider store={ store } >
        <UsuarioState>
            <AlertaState>
                <AuthState> 
                    <Router>
                        <Switch>
                            <Login />
                        </Switch>
                    </Router>           
                </AuthState>
            </AlertaState> 
        </UsuarioState>      
    </Provider>
)

describe('Pruebas en <Login />', () => {

    test('debe mostartse correctamente', () => {
        
        expect( wrapper).toMatchSnapshot();
    });

    test('debe de llamar el dispatch del login', () => {

        wrapper.find('input[name="email"]').simulate('change', {
            target : {
                name: 'email',
                value: 'musa@musa.com',
            }
        });

        wrapper.find('input[name="password"]').simulate('change', {
            target : {
                name: 'password',
                value: '123456',
            }
        });

        wrapper.find('form').at(0).prop('onSubmit')({
            preventDefault(){}
        });

       // expect( iniciarSesion ).toHaveBeenCalledWith('musa@musa.com', '123456');
    })

    describe('../../../components/auth/Login', () => {
        it('should work', () => {
            const spy = sinon.spy();
            const wrapper = shallow(
                <AlertaState>
                    <Login 
                        iniciarSesion={spy}
                    />
                </AlertaState>
                
            );
            wrapper.find('input[name="email"]').to.have.simulate('change', {
                target : {
                    name: 'email',
                    value: 'musa@musa.com',
                }
            });
    
            wrapper.find('input[name="password"]').simulate('change', {
                target : {
                    name: 'password',
                    value: '123456',
                }
            });
    
            wrapper.find('form').at(0).prop('onSubmit')({
                preventDefault(){}
            });
    
           expect( iniciarSesion ).toHaveBeenCalledWith('musa@musa.com', '123456');

            console.log(wrapper.debug());
        })
    })
    
})