import React, { useState, useContext, useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
//import Link from '@material-ui/core/Link';
import Paper from '@material-ui/core/Paper';
import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';

import { Link } from 'react-router-dom';
import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/autentificacion/authContext';

function Copyright() {
  return (
    
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link to="https://material-ui.com/" color="inherit">
        The Last Paper
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  root: {
    height: '100vh',
  },
  image: {
    backgroundImage: 'url(http://www.chiledesarrollosustentable.cl/wp-content/uploads/2013/12/teniente-codelco.jpg)',
    //backgroundImage: 'url(https://www.elrancaguino.cl/wp-content/uploads/2018/04/2vista-planta.jpg)',
    backgroundRepeat: 'no-repeat',
    backgroundColor:
      theme.palette.type === 'light' ? theme.palette.grey[50] : theme.palette.grey[900],
    backgroundSize: 'cover',
    backgroundPosition: 'center',
  },
  paper: {
    margin: theme.spacing(8, 4),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
    //backgroundColor: 'ligth'
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  register: {
    fontSize: 12
  },
  placeholder: {
    fontSize: 12
  }
}));



  export const Login = (props) => {

   export const alertaContext = useContext(AlertaContext);
   export  const { alerta, mostrarAlerta } = alertaContext;

    const authContext = useContext(AuthContext);
    const { mensaje, autenticado, iniciarSesion } = authContext;

    //En caso de que el usuarioo password no exista
    //un registro duplicado
    useEffect(() => {
      if(autenticado) {

        //mostrarAlerta("Ingreso ok", "alerta-ok");
          //aca redijiremos al usuario autenticad a la pag de proyectos
          props.history.push('/proyectos');
          
      }

      if(mensaje) {
          mostrarAlerta(mensaje.msg, mensaje.categoria);
      }

      /*
      if(!autenticado){
        //mostrarAlerta("el usuario no existe o la contraseña es incorrecta", "alerta-error");
      }
      */
      // eslint-disable-next-line
  }, [mensaje, autenticado, props.history])

  //State para iniciar sesión
  const [usuario, guardarUsuario] = useState({
      email:'',
      password: ''
  });
    
  
  //extraer de usuario
  const { email, password} = usuario;
  
  //aqui vamos a ir guardando en el State
  const onChange = e => {
      guardarUsuario({
          //traemos la copia de usuario para lo que este en el State no se 
          //sobre escriba en otra pieza de esta
          //para reescribir la actual -> [e.target.name] : e.target.value
          ...usuario,
          [e.target.name] : e.target.value
      });
  }

  //Cuando el usuario quiere iniciar sesion
  const onSubmit = e => {
      e.preventDefault();

      //Validar que no haya campos vacios
      if(email.trim() === '' || password.trim() || '') {
        //mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
    }

    //nueva alerta por passwords diferentes
    if(password.length < 6) {
      mostrarAlerta("el password debe contener 6 caracteres", "alerta-error")
    } else {
      mostrarAlerta("Ingreso exitoso", "alerta-ok");
    }
    
    //Pasarlo al action si todo esta bien
    iniciarSesion({ email, password });
    
      
      //Pasarlo al action
  }
    
    const classes = useStyles();

    return (
      
      <Grid container component="main" className={classes.root}>
        
      <CssBaseline />
      
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h2">
            Ingreso de Usuario Registrados
          </Typography>
          { alerta ? ( <div className={`alerta ${alerta.categoria}`}> {alerta.msg} </div> )  : null }
          <form className={classes.form} 
            onSubmit={onSubmit}
          >
            <TextField className={classes.placeholder}
              type="email"
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Correo Electrónico"
              name="email"
              value={email}
              //autoComplete="email"
              autoFocus
              onChange={onChange}
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              value={password}
              label="Password"
              type="password"
              id="password"
              //autoComplete="current-password"
              onChange={onChange}
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Ingresar
            </Button>
            <Grid container>
              <Grid item xs>
                <Link to="/registro" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid  className={classes.register} item>
                <Link to={'/registro'} variant="body2">
                  {"No tienes cuenta? Registrate acá"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>



        </div>
      </Grid>
    </Grid>
    );
  }

  export default Login;



/*
export default function SignInSide() {
  const classes = useStyles();

  return (
    <Grid container component="main" className={classes.root}>
      <CssBaseline />
      <Grid item xs={false} sm={4} md={7} className={classes.image} />
      <Grid item xs={12} sm={8} md={5} component={Paper} elevation={6} square>
        <div className={classes.paper}>
          <Avatar className={classes.avatar}>
            <LockOutlinedIcon />
          </Avatar>
          <Typography component="h1" variant="h5">
            Sign in
          </Typography>
          <form className={classes.form} noValidate>
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              id="email"
              label="Email Address"
              name="email"
              autoComplete="email"
              autoFocus
            />
            <TextField
              variant="outlined"
              margin="normal"
              required
              fullWidth
              name="password"
              label="Password"
              type="password"
              id="password"
              autoComplete="current-password"
            />
            <FormControlLabel
              control={<Checkbox value="remember" color="primary" />}
              label="Remember me"
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              color="primary"
              className={classes.submit}
            >
              Sign In
            </Button>
            <Grid container>
              <Grid item xs>
                <Link href="#" variant="body2">
                  Forgot password?
                </Link>
              </Grid>
              <Grid item>
                <Link href="#" variant="body2">
                  {"Don't have an account? Sign Up"}
                </Link>
              </Grid>
            </Grid>
            <Box mt={5}>
              <Copyright />
            </Box>
          </form>
        </div>
      </Grid>
    </Grid>
  );
}

*/