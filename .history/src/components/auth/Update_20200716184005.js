import React, { useState, useContext,  useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
//import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

import AlertaContext from '../../context/alertas/alertaContext';
import AuthContext from '../../context/autentificacion/authContext';
import UsuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';

import MenuItem from '@material-ui/core/MenuItem';
function Copyright() { 
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})); 

export const Registro = (props) => {
    const [users, setUsers] = useState([])

    //extraer los valores del context
    //use context permite acceder a todas las funciones y al state
    const alertaContext = useContext(AlertaContext);
    const { alerta, mostrarAlerta } = alertaContext;

    const authContext = useContext(AuthContext);
    const { mensaje, autenticado, registrarUsuario } = authContext;

    const usuarioContext = useContext(UsuarioContext);
    const { usuarios } = usuarioContext;
    useEffect (() => {
        
        obtenerDatos()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const actualizarUsuarios = async () => {
        const id = props.match.params.id
        const resultado = await clienteAxios.post('/api/usuarios/'+resultado.data.users.id);
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    //En caso de que el usuario se haya autenticado o registrado o sea
    //un registro duplicado
    useEffect(() => {
        if(autenticado) {
            //aca redijiremos al usuario autenticad a la pag de proyectos
            props.history.push('/proyectos');
        }

        if(mensaje) {
            mostrarAlerta(mensaje.msg, mensaje.categoria);
        }
        // eslint-disable-next-line
    }, [mensaje, autenticado, props.history])


    useEffect(() => {
      if(!usuarios){

      }else {
        registrarUsuario(usuarios)
      }
    })
    //State para iniciar sesión
    //los states tiene que tener el mismo nombre que name ej name=nombre
    const [usuario, guardarUsuario] = useState({
        //rut: '',
        nombre: '',
        apellido: '',
        //ciudad: '',
        //telefono: '',
        cargo: '',
        email:'',
        password: '',
        confirmar: '',
        editing: false
    });

    //extraer de usuario
    const { nombre, apellido, cargo, email, password, confirmar } = usuario;

    //aqui vamos a ir guardando en el State
    const onChange = e => {
        guardarUsuario({
            //traemos la copia de usuario para lo que este en el State no se 
            //sobre escriba en otra pieza de esta
            //para reescribir la actual -> [e.target.name] : e.target.value
            ...usuario,
            [e.target.name] : e.target.value
        });
    }

    //Cuando el usuario quiere iniciar sesion
    const onSubmit = e => {
        e.preventDefault();

        //Validar que no haya campos vacios
        if( nombre.trim() === '' || apellido.trim() === '' || cargo.trim() === '' || email.trim() === '' || password.trim() === '' || 
        confirmar.trim() === '' ) {
            //mostrarAlerta('Todos los campos son obligatorios', 'alerta-error');
            return;
        }
        
        //Password minimo de 6 caracteres
        if(password.length < 6) {
            mostrarAlerta('El password debe ser de al menos 6 caracteres', 'alerta-error');
            return;
        }
        //Los dos passwors son iguales
        if(password !== confirmar) {
            mostrarAlerta('Los passwords no son iguales', 'alerta-error');
            return;
        }
        
        //Pasarlo al 
        registrarUsuario({
            nombre,
            apellido,
            //ciudad,
           //telefono,
            cargo, 
            email,
            password
        });
    }
    

    const profesion = [
      {
        value: 'Técnico en Prevención de Riesgos',
        label: 'Técnico en Prevención de Riesgos'
      },
      {
        value: 'Prevencionista de Riesgos',
        label: 'Prevencionista de Riesgos'
      },
      {
        value: 'Jefe de SSOMA',
        label: 'Jefe de SSOMA'
      },
    ];

    function Update(id){
        console.log(id)
        props.history.push("/resgistros/"+id)
    }
    
    

   const classes = useStyles();

    return (

        <Container component="main" maxWidth="xs">
          
      <CssBaseline />

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h2"
          fontWeight="fontWeightBold"
        >
          Registro de Usuarios
        </Typography>

        <form className={classes.form} //noValidate
            onSubmit={onSubmit}
        >
          <div>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                htmlFor="nombre"
                type="text"
                //autoComplete="fname"
                name="nombre"
                variant="outlined"
                required
                fullWidth
                id="nombre"
                label="Nombre"
                autoFocus
                value={nombre}
                onChange={onChange}
                >
               
              </TextField>
              
            </Grid>
            
            <Grid item xs={12} sm={6}>
            
              <TextField
                htmlFor="apellido"
                type="text"
                variant="outlined"
                required
                fullWidth
                id="apellido"
                label="Apellido"
                name="apellido"
                autoComplete="lname"
                value={apellido}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="cargo"
                variant="outlined"
                required
                fullWidth
                id="cargo"
                select
                label="Cargo"
                name="cargo"
                autoComplete="cargo"
                value={cargo}
                onChange={onChange}
              >
                 {profesion.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="email"
                type="email"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                value={email}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="password"
                type="password"
                variant="outlined"
                //required
                fullWidth
                name="password"
                label="Password"
                //type="password"
                id="password"
                //validator={['required']}
                //errorMessages={['this field is required']}
                value={password}
                onChange={onChange}
                //autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="confirmar"
                type="password"
                variant="outlined"
                //required
                fullWidth
                id="confirmar"
                name="confirmar"
                label="Confirmar Password"            
                //autoComplete="confirmar"
                value={confirmar}
                onChange={onChange}
              />
            </Grid>
            
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            input type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            value="Registrarme"
            size="large"
          >
            Registrarme
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="/login" variant="body2">
                Aún no tienes tu Cuenta? Sign in
              </Link>
            </Grid>
          </Grid>
          </div>
          

        </form>

      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>

    );
}

export default Registro;