import React, { useReducer } from 'react';
import alertaReducer from '../../hooks/alerta/alertaReducer';
import alertaContext from '../../hooks/alertas/alertaContext';
//import alertaContext from '../../hooks../alerta/alertaContext';

import { index } from '../../../types';

export const AlertaState = props => {
    const initialState = {
        alerta : null
    }

    const [ state, dispatch ] = useReducer(alertaReducer, initialState);

    //Funciones
    //esta alerta toma dos parametros, msg y categoria
    const mostrarAlerta = (msg, categoria) => {
        dispatch({
            type: index.MOSTRAR_ALERTA,
            payload: {
                msg,
                categoria
            }
        });
        //despues de 5 seg -> un segundo dispatch
        setTimeout(() => {
            dispatch({
                type: index.OCULTAR_ALERTA
            })
            //5000 significa que despues de 5 segundos desaparecerá la alerta
        }, 2500);
    }

    return (
        <alertaContext.Provider
            value={{
                alerta: state.alerta,
                mostrarAlerta
            }}
        >
            {props.children}
        </alertaContext.Provider>
    )
}

export default AlertaState;