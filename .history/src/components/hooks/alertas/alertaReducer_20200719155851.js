import { index } from '../../types';


export default (state, action) => {
    switch (action.type) {
        case index.MOSTRAR_ALERTA:
            return {
                alerta: action.payload
            }
        case index.OCULTAR_ALERTA:
            return {
                alerta: null
            }              
        default:
            return state;
    }
}