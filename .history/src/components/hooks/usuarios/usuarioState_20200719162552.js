import React, { useReducer } from 'react';
import usuarioContext from './usuarioContext';

import usuarioReducer from './usuarioReducer'; 
//import {v4 as uuidv4} from 'uuid';


//importamos los types
import { index } from '../../../types';
import clienteAxios from '../../../config/axios'

//import clienteAxios from '../../config/axios';

//en gral esto es un hook
//agregamos nueva variable proyectoState que sera el state inicial
const UsuarioState = props => {

    //aca se cargan todos los states que estan en el inicio
    const initialState = {
        usuarios : [],
        formulario : false,
        errorformulario: false,
        //aca proyecto será la tarea selecionada en el menu de la izq.
        //usuario: null,
        mensaje: null
    }
             

    //dispatch para ejecutar las acciones o types, es un hook
    //useReducer es similar a useState
    const [state, dispatch] = useReducer(usuarioReducer, initialState)

    //Serie de funciones para el CRUD
    const mostrarFormulario = () => {
        dispatch({
            type: index.FORMULARIO_USUARIOS
        })
    }

    //Obtener todos los usuarios
    const obtenerUsuarios = async () => {
       try {
            const resultado = await clienteAxios.get('/api/usuarios');
            
            dispatch({
                type: index.OBTENER_USUARIOS,
                payload: resultado.data.usuarios
            }) 
       } catch (error) {
            const alerta ={
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }

            dispatch({
                type: index.PROYECTO_ERROR,
                payload: alerta        
            })  
        }
    }
    /*
    const usuarioActual = usuarioId => {
        dispatch({
            type: index.USUARIO_ACTUAL,
            payload: usuarioId
        })
    }
    */
    // Obtener un usuario
    const obtenerUsuario = async usuarioId => {
        try {
             const resultado = await clienteAxios.get('/api/usuarios', 
             {params: { usuarioId }});
             
             dispatch({
                 type: index.OBTENER_USUARIOS,
                 payload: resultado.data.usuarios
             }) 
        } catch (error) {
             const alerta ={
                 msg: 'Hubo un error',
                 categoria: 'alerta-error'
             }
 
             dispatch({
                 type: index.PROYECTO_ERROR,
                 payload: alerta        
             })  
         }
     }



    //Agregar nuevo proyecto
    const agregarUsuario = async usuario => {
        //Aca se agrega el id
        //proyecto.id = uuidv4();

        try {
            const resultado = await clienteAxios.post('/api/usuarios', usuario );
            console.log(resultado);
            //Insertar el poyecto en el state
            dispatch({               
                type: index.AGREGAR_USUARIO,
                payload: resultado.data
            })
        } catch (error) {
            const alerta ={
                msg: 'Hubo un error',
                categoria: 'alerta-error'
            }

            dispatch({
                type: index.PROYECTO_ERROR,
                payload: alerta
              
            })
        }
    }

    //Validar el formulario por errores
    const mostrarError = () => {
        dispatch({
            type: index.VALIDAR_FORMULARIO
        })
    }

    // funcion que Selecciona la Tarea que el usuario hizo click
    //proyectoActual le pasaremos como payload-> proyecto en un arraw fucntion
    const usuarioActual = usuario => {
        dispatch({
            type: index.USUARIO_ACTUAL,
            payload: usuario
        })
    }

    //elimina un proyecto
    const eliminarUsuario = async usuarioId => {
          try {
              await clienteAxios.delete(`/api/usuarios/${usuarioId}`);
              dispatch({
                type: index.ELIMINAR_USUARIO,
                payload: usuarioId
            }) 

          } catch (error) {
              const alerta = {
                  msg: 'Hubo un error',
                  categoria: 'alerta-error'
              }

              dispatch({
                  type: index.PROYECTO_ERROR,
                  payload: alerta
                  //console.log(error);  
              })
              
          }
    }


    //Edita o modifica un usuario
    const actualizarUsuarios = async usuario => {
        console.log(usuario);
        try {
            const resultado = await clienteAxios.put(`/api/usuarios/${usuario._id}`, usuario);
            console.log(resultado);

            dispatch({
                type: index.USUARIO_ACTUAL,
                payload: resultado.data.usuario
            })
        } catch (error) {
            console.log(error);
        }
    }

    const guardarUsuarioActual = usuario => {
        dispatchEvent({
            type: index.USUARIO_ACTUAL,
            payload: usuario
        })
    }
    
    return (
        //desde aqui nacen los datos
        //le pasamos props.children para lo que le vallamos a pasar
        //los diferentes componentes que sean hijos del provider
        //se pasen los datos a lo largo de todos los diferenrtes componentes
        //formulacio = state; mostrarFormulario = funcion
        <usuarioContext.Provider
            value={{
                //se recomienda poner los states arriba y las funciones abajo
                usuarios: state.usuarios,
                formulario: state.formulario,
                errorformulario: state.errorformulario,
                //proyecto contine el valor que tendra del state.proyecto
                usuario: state.usuario,
                mensaje: state.mensaje,
                mostrarFormulario,
                usuarioActual,
                obtenerUsuarios,
                obtenerUsuario,
                agregarUsuario,
                mostrarError,
                //proyectoActual,
                eliminarUsuario,
                actualizarUsuarios,
                guardarUsuarioActual,
                
            }}      
        
        >
             {props.children}

        </usuarioContext.Provider>
           
       
    )
}

export default UsuarioState;