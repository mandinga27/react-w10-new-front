import React, { useState, useEffect, Fragment, useContext } from 'react';
import { Image, List, Table, Header, Icon } from 'semantic-ui-react'

import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
        const userContext = useContext(usuarioContext);
        const { eliminarUsuario } = userContext;
        const [users, setUsers] = useState([])


    useEffect (() => {
        console.log('useEffect')
        obtenerDatos()

    }, [])
  

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
    }
    
    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }
    
    return (
        <Fragment>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Cargo</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <button     
                type="button"
                className="btn btn-eliminar"
                onClick={onClickEliminar(item._id)}
            >Eliminar Usuario &times;</button>
            
                <Icon color='green' name='edit' />
                <Icon color='red' name='user delete' onClick={onClickEliminar(item._id)}/>
                
                
            </Table.Row>
            
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


export default ListUsers;

 
