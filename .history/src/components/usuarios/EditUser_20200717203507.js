import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import clienteAxios from '../../config/axios';

export default class EditUser extends Component {
    constructor(props) {
        super(props);

        this.onChangeNombre = this.onChangeNombre.bind(this);
        this.onChangeApellido = this.onChangeApellido.bind(this);
        this.onChangeCargo = this.onChangeCargo.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onChangeConfirmar = this.onChangeConfirmar.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            nombre: '',
            apellido: '',
            cargo: '',
            email: '',
            password: '',
            confirmar: '',
            users: []
        
    }
}

componentDidMount() {
    clienteAxios.get('http://localhost:5000/exercises/'+this.props.match.params.id)
      .then(response => {
        this.setState({
          nombre: response.data.nombre,
          apellido: response.data.apellido,
          cargo: response.data.cargo,
          email: response.data.email,
        })   
      })
      .catch(function (error) {
        console.log(error);
      })

    clienteAxios.get('api/usuarios/')
      .then(response => {
        if (response.data.length > 0) {
          this.setState({
            users: response.data.map(user => user.username),
          })
        }
      })
      .catch((error) => {
        console.log(error);
      })

  }

  onChangeNombre(e) {
    this.setState({
      nombre: e.target.value
    })
  }

  onChangeApellido(e) {
    this.setState({
      apellido: e.target.value
    })
  }

  onChangeEmail(e) {
    this.setState({
      cargo: e.target.value
    })
  }

  onChangeCargo(e) {
    this.setState({
      email: e.target.value
    })
  }

  onSubmit(e) {
    e.preventDefault();

    const usuario = {
      nombre: this.state.nombre,
      apellido: this.state.apellido,
      cargo: this.state.cargo,
      email: this.state.email,
    }

    console.log(usuario);

    clienteAxios.post('api/usuarios/' + this.props.match.params.id, usuario)
      .then(res => console.log(res.data));

    window.location = '/';
  }
render() {
    return (
        <Segment>
            <div>
            <h3>Editar un Usuario</h3>
            <form onSubmit={this.onSubmit}>
            <div className="form-group">
                    <label>Nombre:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.nombre}
                        onChange={this.onChangeNombre}
                    />

                </div>
                <div className="form-group">
                    <label>Apellido:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.apellido}
                        onChange={this.onChangeApellido}
                    />

                </div>
                <label>Cargo: </label>
                        <select
                        required
                        className="form-control"
                        value={this.state.cargo}
                        onChange={this.onChangeCargo}>
                    {
                        this.state.users.map(function(cargo) {
                            return <option
                                key={cargo}
                                value={cargo}>{cargo}
                                </option>;
                        })
                    }
                    </select>
                
                <div className="form-group">
                    <label>Email:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.email}
                        onChange={this.onChangeEmail}
                    />
                </div>
                <div className="form-group">
                    <label>Password:</label>
                    <input 
                        type="password"
                        required
                        className="form-control"
                        value={this.state.password}
                        onChange={this.onChangePassword}
                    />
                </div>
                <div className="form-group">
                    <label>Confirmar Password:</label>
                    <input 
                        type="password"
                        required
                        className="form-control"
                        value={this.state.confirmar}
                        onChange={this.onChangeConfirmar}
                    />
                </div>
                <div className="form-group">
                    <input type="submit" value="Editar Usuario" className="btn btn-primary" />

                </div>
            </form>
        </div>
        </Segment>
        
        )
    }
}
