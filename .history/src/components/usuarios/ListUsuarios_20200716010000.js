import React, { useState, useEffect, Fragment, useContext } from 'react';
import { Link } from 'react-router-dom';
import { Table, Header, Icon } from 'semantic-ui-react'

import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
        const userContext = useContext(usuarioContext);
        const { eliminarUsuario } = userContext;
        const [ usuario, actualizarUsuarios ] = useState(false);
        const [users, setUsers] = useState([])


    useEffect (() => {
        console.log('useEffect')
        obtenerDatos()

    }, [])
  

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
    }
    
    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }

    const onClickActualizar = (id) => e => {
        e.preventDefault()
        actualizarUsuarios(true)
        console.log(id)
    }
    
    return (
        <Fragment>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Acciones</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <button     
                    type="button"
                    className="small ui button red"
                    onClick={onClickEliminar(item._id)}
                >Eliminar Usuario &times;</button>
                <Link     
                    type="button"
                    className="small ui button green"
                    onClick={onClickActualizar(item._id)}
                >Editar Usuario &times;</Link>
            
                <Icon color='green' name='edit' onClick={onClickActualizar(item._id)}/>
                <Icon color='red' name='user delete' onClick={onClickEliminar(item._id)}/>
                
                
            </Table.Row>
            
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


export default ListUsers;

 
