import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios from '../../config/axios';

/*
const User = props => (
    <tr>
        <th>props.user.nombre</th>
        <th>props.user.apellido</th>
        <th>props.user.cargo</th>
        <th>props.user.email</th>
        <td>
            <Link to={"/edit/"+props.user._id}>edit</Link> | <a href="#" onClick={() => {props.eliminarUsuario(props.user._id)}}>delete</a>
        </td>
    </tr>
)*/

export default class ListUser extends Component {
    constructor(props) {
        super(props);

        this.eliminarUsuario = this.eliminarUsuario.bind(this);

        this.state = {
            nombre: '',
            apellido: '',
            email:'',
            cargo: '',
            users: []
        }
    }

    async componentDidMount() {
        const res = await clienteAxios.get('api/usuarios/');
        console.log(res);
        this.setState({users: res.data});
        // this.setState({
        //     users: res.data.map(user => user.nombre),
        //     userSelected: res.data[0].nombre
        // })
        //     .then(res => {
        //         this.setState({ users: res.data});
        //     }) 
        //     .catch((error) => {
        //         console.log(error);               
        //         //console.log(this.state.users);
        //     })
    }

    eliminarUsuario(id) {
        clienteAxios.delete('api/usuarios/'+id)
            .then(response => {console.log(response.data)});

        this.setState({
            users: this.state.users.filter(el => el._id !== id)
        })
    }
    /*
    userList() {
        return this.state.users.map(item => {
            return <User user={item.nombre} eliminarUsuario={this.eliminarUsuario} key={item._id}/>;
        })
    }*/

    render() {
        return (
            <div>
                <h3>You are on the list users</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        
                    
                    </tbody>

                </table>
            </div>
        )
    }
}