import React, { useState, useEffect, Fragment, useContext} from 'react';
import Navbar from 'react-bootstrap/Navbar';
import { Link } from 'react-router-dom';
import { Table, Header, Icon, Card } from 'semantic-ui-react'
import { Button, Confirm } from 'semantic-ui-react';
import { Modal } from "semantic-ui-react";
import { faList } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
    const userContext = useContext(usuarioContext);
    const { eliminarUsuario } = userContext;
    const [ usuario, actualizarUsuarios ] = useState(false);
    const [users, setUsers] = useState([])

    useEffect (() => {
        
        obtenerDatos()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }

    const Update = (id) => {
       
        console.log(id)
        //props.history.push("/update/"+id)
        obtenerDatos()
    }

    return (
        
        <Fragment>
            <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
                <Navbar.Brand href="#home">React-Bootstrap</Navbar.Brand>
                <Navbar.Toggle aria-controls="responsive-navbar-nav" />
                <Navbar.Collapse id="responsive-navbar-nav">
                    <Nav className="mr-auto">
                    <Nav.Link href="#features">Features</Nav.Link>
                    <Nav.Link href="#pricing">Pricing</Nav.Link>
                    <NavDropdown title="Dropdown" id="collasible-nav-dropdown">
                        <NavDropdown.Item href="#action/3.1">Action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.2">Another action</NavDropdown.Item>
                        <NavDropdown.Item href="#action/3.3">Something</NavDropdown.Item>
                        <NavDropdown.Divider />
                        <NavDropdown.Item href="#action/3.4">Separated link</NavDropdown.Item>
                    </NavDropdown>
                    </Nav>
                    <Nav>
                    <Nav.Link href="#deets">More deets</Nav.Link>
                    <Nav.Link eventKey={2} href="#memes">
                        Dank memes
                    </Nav.Link>
                    </Nav>
                </Navbar.Collapse>
            </Navbar>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Acciones</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row key={item._id}>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Modal 
                    trigger={<Button
                        className="small ui button red"
                    >Eliminar Usuario</Button>}
                    header='Alerta'
                    content="Estas seguro de que quieres eliminar al usuario?"
                    actions={['Cancelar', {key: 'done', content: 'Done', positive: true}]}
                    onClick={onClickEliminar(item._id)}   
                />
                <Link to={"/registros/" + item._id}    
                    type="button"
                    className="small ui button green"
                    onClick={() =>Update(item._id)}
                   
                >Editar Usuario &times;</Link>
                
                <Icon color='green' name='edit'onClick={() =>Update(item._id)} />
                <Icon color='red' name='user delete' onClick={onClickEliminar(item._id)}/>
                
                
            </Table.Row>
            
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


/*

class ListUsers extends React.Component  {
    

    render () {

        constructor(props){
            super(props);
            this.state = {
                users : []
            };
        }
        
        componentDidMount() {
            clienteAxios.get("/api/usuarios")
            .then(response => response.data)
            .then((data) => {
                this.setState({users: data});
            });
        }

        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header><FontAwesomeIcon icon={faList} /> User Lists</Card.Header>
                <Card.Body>
                    <Table bordered hover striped variant="dark">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Autor</th>
                                <th>Email</th>
                                <th>Cargo</th>
                                <th>Acctions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr align="center">
                                <td colSpan="6">No users Available</td>
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
            
        );       
    }
}
*/

 export default ListUsers;
