import React from 'react';
//import Sidebar from '../layout/Sidebar';
//import Barra from '../layout/Barra';
import FormUsuarios from '../usuarios/FormUsuario';
import ListadoUsuarios from '../usuarios/ListadoUsuarios';
//import AuthContext from '../../context/autenticacion/authContext';

const Usuarios = () => {

    // Extraer la información de autenticación
    //const authContext = useContext(AuthContext);
    //const { usuarioAutenticado } = authContext;
    /*
    useEffect(() => {
        usuarioAutenticado();
        // eslint-disable-next-line
    }, [])
    */
    return ( 
        <div className="contenedor-app">
            

            <div className="seccion-principal">
               

                <main>
                    <FormUsuarios />

                    <div className="contenedor-tareas">
                        <ListadoUsuarios />
                    </div>
                </main>
            </div>
        </div>
     );
}
 
export default Usuarios;