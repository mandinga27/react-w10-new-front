import React from 'react';
//import Sidebar from '../layout/Sidebar';
//import Barra from '../layout/Barra';
import FormUsuarios from '../usuarios/FormUsuario';
import ListadoUsuarios from '../usuarios/ListadoUsuarios';
//import AuthContext from '../../context/autenticacion/authContext';
import clienteAxios  from '../../config/axios';

const Usuarios = /*async*/ ({match}) => {

        console.log(match.params.id, 'probando');
        const resultado = await clienteAxios.get('/api/usuarios',);
        //const users = await resultado.json()

        //setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    

    // Extraer la información de autenticación
    //const authContext = useContext(AuthContext);
    //const { usuarioAutenticado } = authContext;
    /*
    useEffect(() => {
        usuarioAutenticado();
        // eslint-disable-next-line
    }, [])
    */
   
   
    return ( 
        <div className="contenedor-app">
            

            <div className="seccion-principal">
               

                <main>
                    <FormUsuarios />

                    <div className="contenedor-tareas">
                        <ListadoUsuarios />
                    </div>
                </main>
            </div>
        </div>
     );
}
 
export default Usuarios;