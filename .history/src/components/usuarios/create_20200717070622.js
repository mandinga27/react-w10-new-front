import React, { Component } from 'react';

export default class CreateUser extends Component {
    constructor(props) {
        super(props);

        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeLname = this.onChangeLname.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangeCargo = this.onChangeCargo.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            lname: '',
            email: '',
            cargo: '',
            users: []
        
    }
}

componentDidMount() {
    this.setState({
        users: ['test user'],
        name: 'test user'
    })
}

onChangeName(e) {
    this.setState({
        name: e.target.value
    });
}

onChangeLname(e) {
    this.setState({
        lname: e.target.value
    });
}

onChangeEmail(e) {
    this.setState({
        email: e.target.value
    });
}

onChangeCargo(e) {
    this.setState({
        cargo: e.target.value
    });
}

onSubmit(e) {
    e.prevenDefault();

    const usuario = {
        name: this.state.name,
        lname: this.state.lname,
        email: this.state.email,
        cargo: this.state.cargo
    }
    console.log(usuario);

    window.location = '/';
}

render() {
    return (
        
        <div>
            <h3>Crear un nuevo usuario</h3>
            <form onsSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Nombre: </label>
                    <select ref="userInput"
                        required
                        className="form-control"
                        value={this.state.name}
                        onChange={this.onChangeName}>
                    {
                        this.state.users.map(function(user) {
                            return <option
                                key={user}
                                value={user}>{user}
                                </option>;
                        })
                    }
                    </select>
                </div>
            </form>
        </div>
    )
}
