import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios from '../../config/axios';


const User = props => (
    <tr>
        <th>props.users.nombre</th>
        <th>props.users.apellido</th>
        <th>props.users.cargo</th>
        <th>props.users.email</th>
        <td>
            <Link to={"/edit/"+props.users._id}>edit</Link> | <a href="#" onClick={() => {props.eliminarUsuario(props.user._id)}}>delete</a>
        </td>
    </tr>
)

export default class ListUser extends Component {
    constructor(props) {
        super(props);

        this.eliminarUsuario = this.eliminarUsuario.bind(this);

        this.state = {users: []}
    }

    componentDidMount() {
        clienteAxios.get('api/usuarios/')
        .then(response => {
            if(response.data.length > 0) {
                this.setState({
                    users: response.data.map(user => user._id)
                    //nombre: response.data[0].nombre
                    
                    
                })
            }
        })
    }
    /*
    async componentDidMount() {
        const res = await clienteAxios.get('api/usuarios/');
        console.log(res);
        this.setState({users: res.data});
        console.log(this.state.users);
        // this.setState({
        //     users: res.data.map(user => user.nombre),
        //     userSelected: res.data[0].nombre
        // })
        //     .then(res => {
        //         this.setState({ users: res.data});
        //     }) 
        //     .catch((error) => {
        //         console.log(error);               
        //         //console.log(this.state.users);
        //     })
    }
    */
    eliminarUsuario(id) {
        clienteAxios.delete('api/usuarios/'+id)
            .then(response => {console.log(response.data)});

        this.setState({
            users: this.state.users.filter(el => el._id !== id)
        })
    }
    
    userList() {
        return this.state.users.map(item => {
            return <User user={item} eliminarUsuario={this.eliminarUsuario} key={item._id}/>;
        })
    }

    render() {
        return (
            <div>
                <br></br>
                <h3>You are on the list users</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Id</th>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {this.userList() }    
                    </tbody>
                </table>
            </div>
        )
    }
}