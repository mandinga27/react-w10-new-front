import React, { useState, useEffect, Fragment, useContext, Component } from 'react';
import { Link } from 'react-router-dom';
import { Table, Header, Icon } from 'semantic-ui-react'
import { Button, Confirm } from 'semantic-ui-react';
import { Modal } from "semantic-ui-react";

import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
        const userContext = useContext(usuarioContext);
        const { eliminarUsuario } = userContext;
        //const [ usuario, actualizarUsuarios ] = useState(false);
        const [users, setUsers] = useState([])
        const [state, setState] = useState()
        const [open, setOpen] = useState(true)
        const [close, setClose] = useState(false)


        


    useEffect (() => {
        console.log(id)
        obtenerDatos()

    }, [])
  

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
    }
    
    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }
    

    /*
    const onClickActualizar = (id) => e => {
        e.preventDefault()
        actualizarUsuarios(true)
        console.log(id)
    }
    */
    return (
        <Fragment>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Acciones</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row key={item._id}>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Modal 
                    trigger={<Button
                        className="small ui button red"
                    >Eliminar Usuario</Button>}
                    header='Alerta'
                    content="Estas seguro de que quieres eliminar al usuario?"
                    actions={['Cancelar', {key: 'done', content: 'Done', positive: true}]}
                    onClick={onClickEliminar(item._id)}   
                />
                <Button
                         
                    type="button"
                    className="small ui button red"
                    onClick={onClickEliminar(item._id)
                    
                    }
                    
                >Eliminar Usuario &times;</Button>

                
                <Link to={"/registros/" + item._id}    
                    type="button"
                    className="small ui button green"
                   
                >Editar Usuario &times;</Link>
            
                <Icon color='green' name='edit' />
                <Icon color='red' name='user delete' onClick={onClickEliminar(item._id)}/>
                
                
            </Table.Row>
            
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


export default ListUsers;

 
