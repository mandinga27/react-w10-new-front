import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Navbar extends Component {

  render() {
    return (
      <nav className="navbar navbar-dark bg-dark navbar-expand-lg">
        <Link to="/" className="navbar-brand">Last Paper</Link>
        <div className="collpase navbar-collapse">
        <ul className="navbar-nav mr-auto">
          <li className="navbar-item">
          <Link to="/list" className="nav-link">Lista de Usuarios</Link>
          </li>
          <li className="navbar-item">
          <Link to="/edit" className="nav-link">Edita Usuario</Link>
          </li>
          <li className="navbar-item">
          <Link to="/registro" className="nav-link">Crear Usuario</Link>
          </li>
          <li className="navbar-item">
          <Link to="/create/" className="nav-link">Crear Usuario</Link>
          </li>
        </ul>
        </div>
      </nav>
    );
  }
}