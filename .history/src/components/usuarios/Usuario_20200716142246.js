import React, { useContext, Fragment } from 'react';
import usuarioContext from '../../context/usuarios/usuarioContext';
import Usuarios from './Usuarios';
import { index } from '../../types';
//import tareaContext from '../../context/tareas/tareaContext';

const Usuario = ({usuario}) => {
    // Obtener el state de proyectos
    //const usuariosContext = useContext(usuarioContext);
    //const { usuarioActual } = usuariosContext;

    // obtener la función del context de tarea
    const usuariosContext = useContext(usuarioContext);
    const { obtenerUsuarios, usuarioActual, guardarUsuarioActual } = usuariosContext;

    // Extraer al usuario
    //const [usuarioActual] = usuario;
    console.log(usuarioActual);

    // Función para agregar el proyecto actual
    const seleccionarUsuario = id => {
        //usuarioActual(id); // Fijar un proyecto actual
        obtenerUsuarios(id); // Filtrar las tareas cuando se de click
    }

    

    return ( 
        <Fragment>
            <li>
                <p>{usuario.nombre}</p>
                <p>{usuario.email}</p>
                <p>{usuario.cargo}</p>
           
            </li>
            <div>
                <button
                    type="button"
                    className=" btn btn-primary"
                >Editar</button>
            </div>
        </Fragment>
        
     );
}

export default Usuario;