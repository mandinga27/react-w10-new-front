import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios from '../../config/axios';

/*
const User = props => (
    <tr>
        <th>props.user.nombre</th>
        <th>props.user.apellido</th>
        <th>props.user.cargo</th>
        <th>props.user.email</th>
        <td>
            <Link to={"/edit/"+props.user._id}>edit</Link> | <a href="#" onClick={() => {props.eliminarUsuario(props.user._id)}}>delete</a>
        </td>
    </tr>
)*/

export default class ListUser extends Component {
    constructor(props) {
        super(props);

        this.eliminarUsuario = this.eliminarUsuario.bind(this);

        this.state = {
            nombre: '',
            apellido: '',
            email:'',
            cargo: '',
            users: []
        }
    }

    componentDidMount() {
        clienteAxios.get('api/usuarios/')
            .then(res => res.json() 
                this.setState({ users: response.data});
                console.log(this.state.users);
            })
            .catch((error) => {
                console.log(error);
                
            })
    }

    eliminarUsuario(id) {
        clienteAxios.delete('api/usuarios/'+id)
            .then(response => {console.log(response.data)});

        this.setState({
            users: this.state.users.filter(el => el._id !== id)
        })
    }
    /*
    userList() {
        return this.state.users.map(item => {
            return <User user={item.nombre} eliminarUsuario={this.eliminarUsuario} key={item._id}/>;
        })
    }*/

    render() {
        return (
            <div>
                <h3>You are on the list users</h3>
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Actions</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            this.state.users.map(item => (
                                
                                    <tr key={item._id}>
                                        <td>{item.nombre}</td>
                                        <td>{item.apellido}</td>
                                        <td>{item.email}</td>
                                        <td>{item.cargo}</td>
                                    </tr>                         
                                ))}
                    </tbody>

                </table>
            </div>
        )
    }
}