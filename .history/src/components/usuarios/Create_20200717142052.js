import React, { Component } from 'react';
import { Segment } from 'semantic-ui-react';
import clienteAxios from '../../config/axios';

export default class CreateUser extends Component {
    constructor(props) {
        super(props);

        this.onChangeNombre = this.onChangeNombre.bind(this);
        this.onChangeApellido = this.onChangeApellido.bind(this);
        this.onChangeCargo = this.onChangeCargo.bind(this);
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            nombre: '',
            apellido: '',
            cargo: '',
            email: '',
            users: []
        
    }
}

componentDidMount() {
    this.setState({
        users: ['--','prevencionista', 'constructor', 'sernageomin'],
        cargo: '--',
        cargo: 'prevencionista',
        cargo: 'constructor',
        cargo: 'sernageominr',
    })
}

onChangeNombre(e) {
    this.setState({
        nombre: e.target.value
    });
}

onChangeApellido(e) {
    this.setState({
        apellido: e.target.value
    });
}

onChangeCargo(e) {
    this.setState({
        cargo: e.target.value
    });
}

onChangeEmail(e) {
    this.setState({
        email: e.target.value
    });
}

onSubmit(e) {
    e.prevenDefault();

    const usuario = {
        nombre: this.state.nombre,
        apellido: this.state.apellido,
        cargo: this.state.cargo,
        email: this.state.email
    }
    console.log(usuario);

    window.location = '/';

    clienteAxios.post('api/usuarios', usuario)
        .then(res => console.log(res.data));
    this.setState({
        nombre: '',
        apellido:'',
        cargo: '',
        email: '',
        password: '',
        confirmar: '',    
    });
}

render() {
    return (
        <Segment>
            <div>
            <h3>Crear un nuevo usuario</h3>
            <form onsSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Nombre: </label>
                        <select
                        required
                        className="form-control"
                        value={this.state.nombre}
                        onChange={this.onChangeNombre}>
                    {
                        this.state.users.map(function(user) {
                            return <option
                                key={user}
                                value={user}>{user}
                                </option>;
                        })
                    }
                    </select>
                </div>
                <div className="form-group">
                    <label>Apellido:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.apellido}
                        onChange={this.onChangeApellido}
                    />

                </div>
                <div className="form-group">
                    <label>Cargo:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.cargo}
                        onChange={this.onChangeCargo}
                    />

                </div>
                <div className="form-group">
                    <label>Email:</label>
                    <input 
                        type="text"
                        required
                        className="form-control"
                        value={this.state.email}
                        onChange={this.onChangeEmail}
                    />
                </div>
                <div className="form-group">
                    <input type="submit" value="Crear Usuario" className="btn btn-primary" />

                </div>
            </form>
        </div>
        </Segment>
        
        )
    }
}
