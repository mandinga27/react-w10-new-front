import React, { Component} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios from '../../config/axios';

export default class ListUser extends Component {
    constructor(props) {
        super(props);

        this.deleteUser = this.deleteUser.bind(this);

        this.state = {users: []};
    }

    componentDidMount() {
        clienteAxios.get('api/usuarios')
            .then(response => {
                this.setState({ users: response.data})
            })
            .catch((error) => {
                console.log(error);
                
            })
    }

    eliminarUsuario(id) {
        clienteAxios.delete('api/usuarios/'+id)
            .then(res => console.log(res.data));
        this.setState({
            users: this.state.users.filter(el => el._id !== id)
        })
    }

    render() {
        return (
            <div>
                <p>You are on the list users</p>
            </div>
        )
    }
}