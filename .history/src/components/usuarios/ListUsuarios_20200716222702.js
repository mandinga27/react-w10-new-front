import React, { useState, useEffect, Fragment, useContext, Component } from 'react';
import { Link } from 'react-router-dom';
import { Table, Header, Icon, Card } from 'semantic-ui-react'
import { Button, Confirm } from 'semantic-ui-react';
import { Modal } from "semantic-ui-react";
import { faList } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
//import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'


class ListUsers extends Component  {
    constructor(props){
        super(props);
        this.state = {
            users : []
        };
    }
    
    componentDidMount() {
        clienteAxios.get("/api/usuarios")
        .then(response => response.data)
        .then((data) => {
            this.setState({users: data});
        });
    }

    render () {

        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header><FontAwesomeIcon icon={faList} /> User Lists</Card.Header>
                <Card.Body>
                    <Table bordered hover striped variant="dark">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Autor</th>
                                <th>Email</th>
                                <th>Cargo</th>
                                <th>Acctions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr align="center">
                                <td colSpan="6">No users Available</td>
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
            
        );       
    }
}


 export default ListUsers;
