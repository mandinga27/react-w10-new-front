import React, { useContext } from 'react';
import usuarioContext from '../../context/usuarios/usuarioContext';
import Usuarios from './Usuarios';
//import tareaContext from '../../context/tareas/tareaContext';

const Usuario = ({usuario}) => {
    // Obtener el state de proyectos
    //const usuariosContext = useContext(usuarioContext);
    //const { usuarioActual } = usuariosContext;

    // obtener la función del context de tarea
    const usuariosContext = useContext(usuarioContext);
    const { obtenerUsuarios } = usuariosContext;

    // Función para agregar el proyecto actual
    const seleccionarUsuario = id => {
        //usuarioActual(id); // Fijar un proyecto actual
        obtenerUsuarios(id); // Filtrar las tareas cuando se de click
    }

    return ( 
        <li>
            <button
                type="button"
                className="btn btn-blank"
                onClick={ () => seleccionarUsuario(usuario._id) }
            >{usuario.nombre} </button>
        </li>
     );
}

export default Usuario;