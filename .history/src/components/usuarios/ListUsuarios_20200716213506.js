import React, { useState, useEffect, Fragment, useContext, Component } from 'react';
import { Link } from 'react-router-dom';
import { Table, Header, Icon, Card } from 'semantic-ui-react'
import { Button, Confirm } from 'semantic-ui-react';
import { Modal } from "semantic-ui-react";
import { faList } from '@fortawesome/free-solid-svg-icons';
//import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../config/axios';




//const baseUrl = 'http://localhost:4000'

export default class ListUsers extends Component  {

    constructor(props){
        super(props);
        this.state = {
            users : []
        };
    }

    componentDidMount() {
        clienteAxios.get("/api/usuarios")
        .then(response => response.data)
        .then((data) => {
            this.setState({users: data});
        });
    }
    render () {
        //const userContext = useContext(usuarioContext);
        //const { eliminarUsuario } = userContext;
        //const [ usuario, actualizarUsuarios ] = useState(false);
        //const [users, setUsers] = useState([])
        //const [modoEdicion, setModoEdicion] = useState(false)


     


        

        /*
    useEffect (() => {
        
        obtenerDatos()
        //console.log(users.data)

    }, [])
  

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        //setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const eliminarUsuario = (usuarioId) => {
        clienteAxios.delete('/api/usuarios'+usuarioId)
        .then(response => {
            if(response.data != null) {
                alert("Usuario eliminado");
                this.setState({
                    usuarios: this.state.usuarios.filter(usuario => usuario._id !== usuarioId)
                });
            }
        });
    };    
    
    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }

    const Update = (id) => {
       
        console.log(id)
        //props.history.push("/update/"+id)
        obtenerDatos()
    }
    

    /*
    const onClickActualizar = (id) => e => {
        e.preventDefault()
        actualizarUsuarios(true)
        console.log(id)
    }
    */
    return (
        <Card className={"border border-dark bg-dark text-white"}>
            <Card.Header><FontawesomeIcon icon={faList} /> User Lists</Card.Header>
            <Card.Body>
                <Table bordered hover striped variant="dark">
                    <thead>
                        <tr>
                            <th>Title</th>
                            <th>Autor</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Acctions</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr align="center">
                            <td colSpan="6">No users Available</td>
                        </tr>
                    </tbody>
                </Table>
            </Card.Body>
        </Card>
        
    );       
    }
}


 
