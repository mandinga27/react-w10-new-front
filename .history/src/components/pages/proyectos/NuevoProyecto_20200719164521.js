import React, { useState, useContext } from 'react';
import proyectoContext from '../../hooks/proyectos/proyectoContext';
//import proyectoContext from '../../context/proyectos/proyectoContext';

import { makeStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Container from '@material-ui/core/Container';
//import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
//import Checkbox from '@material-ui/core/Checkbox';
//import Box from '@material-ui/core/Box';
import Grid from '@material-ui/core/Grid';
import CssBaseline from '@material-ui/core/CssBaseline';
//import { makeStyles } from '@material-ui/core';
//import Contenedor from '../layout/Contenedor';


const NuevoProyecto = () => {

    //Obtener el state del formulario sin props con useContext
    const proyectosContext = useContext(proyectoContext);
    //aca vemos el state, inicalmente en false, formulario fue creado en proyectoState.js
    //aca tambien se extraen las funciones
    //se recomienda extraer primero los staes y luego las funciones
    const { formulario, errorformulario, mostrarFormulario, agregarProyecto, mostrarError } = proyectosContext;

    //State para Tareas
    const [proyecto, guardarProyecto ] = useState({
        nombre: ''
    });

    //Extraer nombre de tarea
    const { nombre } = proyecto;

    //Lee los contenidos del input
    const onChangeProyecto = e => {
        guardarProyecto({
            ...proyecto,
            [e.target.name] : e.target.value
        })
    }

    //Cuando el usuario envía un proyecto o cuando el usuario hace submit
    const onSubmitProyecto = e => {
        e.preventDefault();

        //Validar el proyecto
        //pasamos la funcion mostrarError()
        /*esa funcion, va ir a state -> proyecto.State.js,
        identifica a VALIDAR_FORMULARIO, luego se va al reducer protectoReducer.js
        identifica VALIDAR_FORMULARIO y cambia el error como true
        al cambiar a true, se va al ternario creado en Fragment
        {errorformulario ? <p className="mensaje error">El nombre del ../>p> : null }
        -> se evalua como verdadera si el campo esta vacio -> errorformulario ? -> muestra el mensaje  
        */
        if(nombre === '') {
            mostrarError();
            return;
        }
        //Agregar al state
        agregarProyecto(proyecto)
        
        //Reiniciar el form, nombre queda como string vacio
        guardarProyecto({
            nombre: ""
        });
    }

    const profesion = [
        {
          value: 'Trabajo en Caliente',
          label: 'Trabajo en Caliente'
        },
        {
          value: 'Trabajo en Alturas',
          label: 'Trabajo en Alturas'
        },
        {
          value: 'Mantención',
          label: 'Mantención'
        },
      ];

    //Mostrar el formulario
    const onClickFormulario = () => {
        mostrarFormulario();
    }
    
    const useStyles = makeStyles((theme) => ({
        form: {
          width: '100%', // Fix IE 11 issue.
          marginTop: theme.spacing(3),
        },
        text: {
            fontSize: 18
        },
      })); 
    
    
      

    /*
    const estilos = makeStyles(theme => ({
        offset: theme.mixins.toolbar,
        root: {
            fontSize: 28,
        },
        text: {
            fontSize: 28,
        },
    }));

    const classes = estilos();
    */

    const classes = useStyles();

    return(
        <Container>
            <CssBaseline />
            <button
                type="button"
                className="btn btn-block btn-primary"
                //aca mostramos el formulario al hacer click

                //{ formulario  ? } -> si formulario existe ->
                onClick={ onClickFormulario }
            >Agregar Nueva Actividad</button>
            
            { formulario ?
                    (
                        <form
                            className="formulario-nuevo-proyecto"
                            onSubmit={onSubmitProyecto}
                        >   
                        <div>
                        <Grid item xs={12}>
                                <TextField 
                                    //htmlFor="postura"
                                    type="text"
                                    className={classes.text} 
                                    placeholder="Nombre de la Tarea"
                                    name="nombre"
                                    fullWidth
                                    //id="nombre"
                                    //value={nombre}
                                    select
                                    varian="outlined"
                                    label="Postura"
                                    //name="postura"
                                    //value={nombre}
                                    onChange={onChangeProyecto}
                                >   
                                    {profesion.map((option) => (
                                        <MenuItem key={option.value} value={option.value}>
                                            {option.label}
                                        </MenuItem>
                                    ))}
                                </TextField>
                            </Grid>
                        </div>

                            
                            <input
                                type="submit"
                                className="btn btn-primario btn-primary"
                                value="Agregar Actividad"
                            />

                        </form>
                    ) : null } 
             { errorformulario ? 
                <h2 className="mensaje error">El nombre de la Actividad es obligatoria</h2>

                : null}     
        </Container>
     
    );

}

export default NuevoProyecto;