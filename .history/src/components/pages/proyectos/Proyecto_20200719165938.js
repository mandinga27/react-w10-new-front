import React, { useContext } from 'react';

import proyectoContext from '../../hooks/proyectos/proyectoContext';
import tareaContext from '../../hooks/tareas/tareaContext';
import { Button } from 'semantic-ui-react';

//vamos a pasar un props ({})

const Proyecto = ({proyecto}) => {

    //Obtener el state de proyectos
    const proyectosContext = useContext(proyectoContext);
    const { proyectoActual } = proyectosContext;
    
    //Obtener la funcion del context de tarea
    const tareasContext = useContext(tareaContext);
    const { obtenerTareas } = tareasContext;

    // Funcion para agregar el proyecto actual y las tareas
    const seleccionarProyecto = id => {
        proyectoActual(id); //Fijar un proyecto actual
        obtenerTareas(id); //Filtrar las tareas cuando se de click

    }
    return (
              
        <li>
            <Button basic color='black' content='Black'
                //margin="top"
                //size='huge'
                onClick={ () => seleccionarProyecto(proyecto._id) }
                > {proyecto.nombre} </Button>
            
        </li>
       
        
        /*<li>
            <button
            hola
                type="button"
                className="btn btn-blank"
                //onClick={ () => seleccionarProyecto(proyecto._id) }
            >{proyecto.nombre} </button>
        </li>*/
        
     );
}

export default Proyecto;