import React, { useState, useContext,  useEffect } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import clienteAxios  from '../../../config/axios';
import usuarioContext from '../../hooks/usuarios/usuarioContext'
//import { ValidatorForm, TextValidator } from 'react-material-ui-form-validator';

//import AuthContext from '../../hooks/autentificacion/authContext'

import AlertaContext from '../../hooks/alertas/alertaContext';
import AuthContext from '../../hooks/autentificacion/authContext';
import ListaUsuarios from './ListaUsuarios';

import MenuItem from '@material-ui/core/MenuItem';
function Copyright() { 
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  root: {
    '& .MuiTextField-root': {
      margin: theme.spacing(1),
      width: '25ch',
    },
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
})); 

const UpdateUser = (props) => {
    console.log(props);
    

    const userContext = useContext(usuarioContext);
    const { eliminarUsuario, actualizarUsuarios } = userContext;
    const [user, setUser] = useState('');
    const [users, setUsers] = useState([])
    const [modoEdicion, setModoEdicion] = useState()
    const [formValues,, SetFormValues] = useState();
    //const { nombre, apellido, email, cargo } = formValues;
    

    useEffect (() => {
        
        obtenerDatos()
        datosUsuarios()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log('desde resultado')
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        const arrayFiltrado = users.filter(item => item.id)
        obtenerDatos()
        
        //console.log(id)
    }

    const datosUsuarios = async () => {
        try {
            const res = await clienteAxios.get('api/usuarios');
            //const data = await res.json()
            setUsers(res.data.users)
            //const usuariosData = data.results.map(item => item);
            console.log('desde datosUsuarios')
            //console.log(usuariosData);
        } catch (error) {
            console.log(error)
        }
    }

    const Update = (id) => {
        console.log(id)
        obtenerDatos()
    }
    const [usuario, editarUsuario] = useState({
      nombre: '',
      apellido: '',
      cargo: '',
      email:'',
      password: '',
      confirmar: ''
  });

  //extraer de usuario
  const { nombre, apellido, cargo, email, password, confirmar } = usuario;

    //aqui vamos a ir guardando en el State
    const onChange = e => {
        setUser({
            ...usuario,
            [e.target.name] : e.target.value
        });
    }

    //Cuando el usuario quiere iniciar sesion
    const onSubmit = e => {
        e.preventDefault();
        
        //Pasarlo al 
        editarUsuario({
            nombre,
            apellido,
            cargo, 
            email,
            password
        });
    }
    

    const profesion = [
      {
        value: 'Técnico en Prevención de Riesgos',
        label: 'Técnico en Prevención de Riesgos'
      },
      {
        value: 'Prevencionista de Riesgos',
        label: 'Prevencionista de Riesgos'
      },
      {
        value: 'Jefe de SSOMA',
        label: 'Jefe de SSOMA'
      },
    ];
  
    /*
   const handleSubmitForm = (e) => {
     e.preventDefault();

     if(true) {
       dispatch ( actualizarUsuarios )
     }

   }*/

   const classes = useStyles();

    return (

        <Container component="main" maxWidth="xs">
      <CssBaseline />

      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>

        <Typography component="h1" variant="h2"
          fontWeight="fontWeightBold"
        >
          Edición de Usuarios
        </Typography>

        <form className={classes.form} //noValidate
            onSubmit={onSubmit}
        >
          <div>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                htmlFor="nombre"
                type="text"
                //autoComplete="fname"
                name="nombre"
                variant="outlined"
                required
                fullWidth
                id="nombre"
                label="Nombre"
                autoFocus
                value={nombre}
                onChange={onChange}
                >
               
              </TextField>
              
            </Grid>
            
            <Grid item xs={12} sm={6}>
            
              <TextField
                htmlFor="apellido"
                type="text"
                variant="outlined"
                required
                fullWidth
                id="apellido"
                label="Apellido"
                name="apellido"
                autoComplete="lname"
                value={apellido}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="cargo"
                variant="outlined"
                required
                fullWidth
                id="cargo"
                select
                label="Cargo"
                name="cargo"
                autoComplete="cargo"
                value={cargo}
                onChange={onChange}
              >
                 {profesion.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="email"
                type="email"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                value={email}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="password"
                type="password"
                variant="outlined"
                //required
                fullWidth
                name="password"
                label="Password"
                //type="password"
                id="password"
                //validator={['required']}
                //errorMessages={['this field is required']}
                value={password}
                onChange={onChange}
                //autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="confirmar"
                type="password"
                variant="outlined"
                //required
                fullWidth
                id="confirmar"
                name="confirmar"
                label="Confirmar Password"            
                //autoComplete="confirmar"
                value={confirmar}
                onChange={onChange}
              />
            </Grid>
            
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            input type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            value="Registrarme"
            size="large"
          >
            Actualizar
          </Button>
          
          </div>
          

        </form>

      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>

    );
}

export default UpdateUser;