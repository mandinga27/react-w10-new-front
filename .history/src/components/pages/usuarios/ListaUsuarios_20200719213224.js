import React, { useState, useEffect, Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios  from '../../../config/axios';
import usuarioContext from '../../hooks/usuarios/usuarioContext'

const ListaUsuarios = () => {   
    const userContext = useContext(usuarioContext);
    const { eliminarUsuario } = userContext;
    const [users, setUsers] = useState([])

    useEffect (() => {
        
        obtenerDatos()
        datosUsuarios()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log('desde resultado')
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const datosUsuarios = async () => {
        try {
            const res = await clienteAxios.get('api/usuarios');
            //const data = await res.json()
            setUsers(res.data.users)
            //const usuariosData = data.results.map(item => item);
            console.log('desde datosUsuarios')
            //console.log(usuariosData);
        } catch (error) {
            console.log(error)
        }
    }

    const Update = (id) => {
        console.log(id)
        obtenerDatos()
    }
  

    return (
        <Fragment>
            <div className="container mt-5">
                <h1 className="text-center">Simple</h1>
                <hr />
                <div className="row">
                    <div className="col-8">
                        <h4 className="text-center">Lista de usuarios</h4>
                        <ul className="list-group">
                            {
                                users.map(item => (
                                    <li className="list-group-item" key={item.key}>
                                        <span className="lead">Nombre del usuario</span>
                                        <button className="btn btn-danger btn-sm float-right mx-2">Eliminar</button>
                                        <button className="btn btn-warning btn-sm float right">Editar</button>
                                    </li>
                                ))
                            }

                        </ul>

                    </div>

                </div>
            </div>
            <div>
            <p>Desde ListaUsuarios</p>
        </div>
        </Fragment>

        
    )

}


export default ListaUsuarios;