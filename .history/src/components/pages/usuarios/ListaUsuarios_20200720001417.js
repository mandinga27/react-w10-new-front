import React, { useState, useEffect, Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import "bootstrap/dist/css/bootstrap.min.css";

import clienteAxios  from '../../../config/axios';
import usuarioContext from '../../hooks/usuarios/usuarioContext'

const ListaUsuarios = () => {   
    const userContext = useContext(usuarioContext);
    const { eliminarUsuario } = userContext;
    const [user, setUser] = useState('');
    const [users, setUsers] = useState([])
    

    useEffect (() => {
        
        obtenerDatos()
        datosUsuarios()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log('desde resultado')
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        const arrayFiltrado = users.filter(item => item.id)
        obtenerDatos()
        
        //console.log(id)
    }

    const datosUsuarios = async () => {
        try {
            const res = await clienteAxios.get('api/usuarios');
            //const data = await res.json()
            setUsers(res.data.users)
            //const usuariosData = data.results.map(item => item);
            console.log('desde datosUsuarios')
            //console.log(usuariosData);
        } catch (error) {
            console.log(error)
        }
    }

    const Update = (id) => {
        console.log(id)
        obtenerDatos()
    }

    const editar = item => {
        console.log('desde editar')
        console.log(item)
    }
  

    return (
        <Fragment>

        <br></br>
            <h3 className="text-center">Lista de usuariosa</h3>
             
            <table className=" table table-border-less">
                <thead>
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Cargo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    </table>
             
                    <table>
                    {
                        users.map(item => (
                             <tr key={item.id}>
                                <td>{item.nombre}</td>
                                <td>{item.apellido}</td>
                                <td>{item.email}</td>
                                <td>{item.cargo}</td>
                                <td>
                                    <button 
                                        className="btn btn-danger btn-sm float-right mx-2"
                                        onClick={onClickEliminar(item._id)}
                                        >Eliminar
                                        </button>
                                        
                                        <Link to={"/editar/" + item._id}>
                                        <button 
                                        className="btn btn-warning btn-sm float right"
                                        onClick={() => editar(item)}
                                        >Editar
                                        </button>
                                        </Link>
                                    
                                </td>     
                            </tr>
                            
                                ))}
                    </table>
                    <form className={classes.form} //noValidate
            onSubmit={onSubmit}
        >
          <div>
          <Grid container spacing={2}>
            <Grid item xs={12} sm={6}>
              <TextField
                htmlFor="nombre"
                type="text"
                //autoComplete="fname"
                name="nombre"
                variant="outlined"
                required
                fullWidth
                id="nombre"
                label="Nombre"
                autoFocus
                value={nombre}
                onChange={onChange}
                >
               
              </TextField>
              
            </Grid>
            
            <Grid item xs={12} sm={6}>
            
              <TextField
                htmlFor="apellido"
                type="text"
                variant="outlined"
                required
                fullWidth
                id="apellido"
                label="Apellido"
                name="apellido"
                autoComplete="lname"
                value={apellido}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="cargo"
                variant="outlined"
                required
                fullWidth
                id="cargo"
                select
                label="Cargo"
                name="cargo"
                autoComplete="cargo"
                value={cargo}
                onChange={onChange}
              >
                 {profesion.map((option) => (
                    <MenuItem key={option.value} value={option.value}>
                      {option.label}
                    </MenuItem>
                ))}
                </TextField>
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="email"
                type="email"
                variant="outlined"
                required
                fullWidth
                id="email"
                label="Email"
                name="email"
                autoComplete="email"
                value={email}
                onChange={onChange}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="password"
                type="password"
                variant="outlined"
                //required
                fullWidth
                name="password"
                label="Password"
                //type="password"
                id="password"
                //validator={['required']}
                //errorMessages={['this field is required']}
                value={password}
                onChange={onChange}
                //autoComplete="current-password"
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                htmlFor="confirmar"
                type="password"
                variant="outlined"
                //required
                fullWidth
                id="confirmar"
                name="confirmar"
                label="Confirmar Password"            
                //autoComplete="confirmar"
                value={confirmar}
                onChange={onChange}
              />
            </Grid>
            
            <Grid item xs={12}>
              <FormControlLabel
                control={<Checkbox value="allowExtraEmails" color="primary" />}
                label="I want to receive inspiration, marketing promotions and updates via email."
              />
            </Grid>
          </Grid>
          <Button
            input type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
            value="Registrarme"
            size="large"
          >
            Actualizar
          </Button>
          
          </div>
          

        </form>   
              
                     
        
        </Fragment>    
    )

}


export default ListaUsuarios;