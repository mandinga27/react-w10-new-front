import React, { useState, useEffect, Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';
import { Table, Header, Icon, Button, Modal, Menu, Segment } from 'semantic-ui-react'


import usuarioContext from '../../hooks/usuarios/usuarioContext';
//import usuarioContext from '../../context/usuarios/usuarioContext';

import clienteAxios  from '../../../config/axios';




//const baseUrl = 'http://localhost:4000'

const ListUsers = () => {
    const userContext = useContext(usuarioContext);
    const { eliminarUsuario } = userContext;
    const [ usuario, actualizarUsuarios ] = useState(false);
    const [users, setUsers] = useState([])

    //navbar
    const [activeItem, setActiveItem] = useState('home');

    const handleItemClick = e => {
        setActiveItem(e.target.value);
    };

    useEffect (() => {
        
        obtenerDatos()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    

    const onClickEliminar = (id) => e => {
        e.preventDefault()
        eliminarUsuario(id)
        obtenerDatos()
        
        //console.log(id)
    }

    const Update = (id) => {
       
        console.log(id)
        //props.history.push("/update/"+id)
        obtenerDatos()
    }

    const editar = item => {
        console.log('desde editar imprimiendo item')
        console.log(item)
        setModoEdicion(true)
        setUsers(item.nombre)
        const destructuring = item
        console.log(destructuring)
        const {nombre, apellido, cargo, email} = item
        console.log(`usuario: ${nombre},
        ${apellido},${cargo},${email}`)
    }

    }


    return (
        
        <Fragment>
            <br></br>
            <Segment inverted>
                <Menu inverted secondary>
                    <Link to={'/'}>
                        <Menu.Item
                            name='home'
                            active={activeItem === 'home'}
                            onClick={handleItemClick}
                        />
                    </Link>
                    <Link to={'/new'}>
                        <Menu.Item
                            name='Registro de Usuario'
                            active={activeItem === 'messages'}
                            onClick={handleItemClick}
                        />
                    </Link>
                
                <Menu.Item
                    name='friends'
                    active={activeItem === 'friends'}
                    onClick={handleItemClick}
                />
                </Menu>
            </Segment>
            <div>
                <br></br>
                <Header as='h1'>Lista de Usuarios Registrados</Header>
            </div>
            <Table singleLine>
                <Table.Header>
                <Table.Row>
                    <Table.HeaderCell>Nombre</Table.HeaderCell>
                    <Table.HeaderCell>Apellido</Table.HeaderCell>
                    <Table.HeaderCell>E-mail address</Table.HeaderCell>
                    <Table.HeaderCell>Cargo</Table.HeaderCell>
                     <Table.HeaderCell>Acciones</Table.HeaderCell>
                    
                </Table.Row>
            </Table.Header>

            <Table.Body>
            {users.map(item => (
            <Table.Row key={item._id}>
                <Table.Cell>{item.nombre}</Table.Cell>
                <Table.Cell>{item.apellido}</Table.Cell>
                <Table.Cell>{item.email}</Table.Cell>
                <Table.Cell>{item.cargo}</Table.Cell>
                <Modal 
                    trigger={<Button
                        className="small ui button red"
                    >Eliminar Usuario</Button>}
                    header='Alerta'
                    content="Estas seguro de que quieres eliminar al usuario?"
                    actions={['Cancelar', {key: 'done', content: 'Done', positive: true}]}
                    onClick={onClickEliminar(item._id)}   
                />
                <Link to={"/usuarioseditar/" + item._id}    
                    type="button"
                    className="small ui button green"
                    onClick={() => editar(item)}
                    //onClick={() =>Update(item._id)}
                   
                >Editar Usuario &times;</Link>
                
                <Icon color='green' name='edit'onClick={() =>Update(item._id)} />
                <Icon color='red' name='user delete' onClick={onClickEliminar(item._id)}/>
                

                
            </Table.Row>
            
            ))}
            </Table.Body>
            </Table>
            
        </Fragment>
              
    )
}


/*

class ListUsers extends React.Component  {
    

    render () {

        constructor(props){
            super(props);
            this.state = {
                users : []
            };
        }
        
        componentDidMount() {
            clienteAxios.get("/api/usuarios")
            .then(response => response.data)
            .then((data) => {
                this.setState({users: data});
            });
        }

        return (
            <Card className={"border border-dark bg-dark text-white"}>
                <Card.Header><FontAwesomeIcon icon={faList} /> User Lists</Card.Header>
                <Card.Body>
                    <Table bordered hover striped variant="dark">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Autor</th>
                                <th>Email</th>
                                <th>Cargo</th>
                                <th>Acctions</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr align="center">
                                <td colSpan="6">No users Available</td>
                            </tr>
                        </tbody>
                    </Table>
                </Card.Body>
            </Card>
            
        );       
    }
}
*/

 export default ListUsers;
