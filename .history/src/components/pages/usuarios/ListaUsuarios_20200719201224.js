import React, { useState, useEffect, Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios  from '../../../config/axios';

const ListaUsuarios = () => {   
    const [users, setUsers] = useState([])

    useEffect (() => {
        
        obtenerDatos()
        datosUsuarios()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log('desde resultado')
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const datosUsuarios = async ({match}) => {
        try {
            const res = await clienteAxios.get('api/usuarios/'+match.params.id);
            //const data = await res.json()
            setUsers(res.data.users)
            //const usuariosData = data.results.map(item => item);
            //console.log('desde datosUsuarios')
            //console.log('desde res',res)
            //console.log(usuariosData);
        } catch (error) {
            console.log(error)
        }
    }
  

    return (

        <div>
            <p>Desde ListaUsuarios</p>
        </div>
    )

}


export default ListaUsuarios;