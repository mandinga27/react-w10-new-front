import React, { useState, useEffect, Fragment, useContext} from 'react';
import { Link } from 'react-router-dom';
import clienteAxios  from '../../../config/axios';
import usuarioContext from '../../hooks/usuarios/usuarioContext'

const ListaUsuarios = () => {   
    const userContext = useContext(usuarioContext);
    const { eliminarUsuario } = userContext;
    const [users, setUsers] = useState([])

    useEffect (() => {
        
        obtenerDatos()
        datosUsuarios()
        //console.log(users.data)

    }, [])

    const obtenerDatos = async () => {
        const resultado = await clienteAxios.get('/api/usuarios');
        //const users = await resultado.json()

        setUsers(resultado.data.users)
        console.log('desde resultado')
        console.log(resultado)
        //console.log(resultado.data.users._id)
    }

    const datosUsuarios = async () => {
        try {
            const res = await clienteAxios.get('api/usuarios');
            //const data = await res.json()
            setUsers(res.data.users)
            //const usuariosData = data.results.map(item => item);
            console.log('desde datosUsuarios')
            //console.log(usuariosData);
        } catch (error) {
            console.log(error)
        }
    }

    const Update = (id) => {
        console.log(id)
        obtenerDatos()
    }
  

    return (
        <Fragment>
            <div>
                <h4 className="text-center">Lista de usuarios</h4>                       
                <table className="table">
                    <thead className="thead-light">
                        <tr>
                            <th>Nombre</th>
                            <th>Apellido</th>
                            <th>Email</th>
                            <th>Cargo</th>
                        </tr>
                            </thead>
                            <tbody>
                            {
                                users.map(item => (
                                    <tr className="list-group-item" key={item.id}>
                                        <td>{item.nombre}</td>
                                        <td>{item.apellido}</td>
                                        <td>{item.email}</td>
                                        <td>{item.cargo}</td>
                                        <button className="btn btn-danger btn-sm float-right mx-2">Eliminar</button>
                                        <button className="btn btn-warning btn-sm float right">Editar</button>
                                    </tr>
                                ))
                            }
                            </tbody>
                        </table>
                    </div>
        </Fragment>

        
    )

}


export default ListaUsuarios;