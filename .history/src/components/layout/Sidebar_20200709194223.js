import React from 'react';
import NuevoProyecto from '../proyectos/NuevoProyecto';
import ListadoProyectos from '../proyectos/ListadoProyectos';

const Sidebar = () => {
    return(
        <aside>
            <h1>Actividades</h1>

            <NuevoProyecto />

            <div className="proyectos">
                <h2>Escoja una Actividad<span> de la lista</span></h2>

                <ListadoProyectos />

            </div>
        </aside>
    );
}

export default Sidebar;