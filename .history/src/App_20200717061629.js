import React, {Component} from 'react';


import Nav from './components/usuarios/Nav';
//importamos BrowserRouter para crear nuestras rutas
import {BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import Login from './components/auth/Login';
import Registro from './components/auth/Registro';
import Proyectos from './components/proyectos/Proyectos';

import Charla from './components/pages/Charla';
import ListUsuarios from './components/usuarios/ListUsuarios';
import Registros from './components/usuarios/Usuarios';
//import ListUsers from './components/usuarios/ListUsuarios';
import usuarioState from './context/usuarios/usuarioState';
import Usuarioss from './components/usuarios/Usuario';
import Update from './components/auth/Update';
import EditUser from './components/usuarios/Edit';


import ProyectoState from './context/proyectos/proyectoState';
import TareaState from './context/tareas/tareaState';
import AlertaState from './context/alertas/alertaState';

import AuthState from  './context/autentificacion/authState';
import tokenAuth from './config/tokenAuth';
import RutaPrivada from './components/rutas/RutaPrivada';
import UsuarioState from './context/usuarios/usuarioState';
import Usuarios from './components/usuarios/Usuarios'; 


//Revisar su tenemos un token
const token = localStorage.getItem('token');
if(token) {
  tokenAuth(token);
}
function App() {
  
  console.log('http://localhost:4000');
  
  return (
    //todo lo que este destro del Switch serán nuestras diferentes paginas
    //todo lo que este por fuera de Switch es lo que se vera en todas las paginas
    //agregamos nuestro ProyectoState para el Provider
    //se poner al inicio para que este disponible para todos los componentes y props  la app
    //similar a redux pero en context
      
      
      //<Navbar />
    
      <ProyectoState>
        <UsuarioState>
        <TareaState>
          <AlertaState>
            <AuthState>          
              <Router>
                <Nav />
                  
                <Switch>                 
                  <Route exact path="/" component={Login} />
                  <Route exact path="/registro" component={Registro} />
                  <Route exact path="/registros/:id" component={Registros} />
                  <Route exact path="/edit/:id" component={EditUser} />

                  <Route exact path="/charla" component={Charla} />
                  <Route exact path="/list" component={ListUsuarios} />
                  <Route exact path="/usuarios" component={Usuarioss} />
                  <Route exact path="/update:id" component={Update} />
                  <RutaPrivada exact path="/proyectos" component={Proyectos} />
                </Switch>
              </Router>
            </AuthState>  
          </AlertaState>     
      </TareaState>
      </UsuarioState>
    </ProyectoState>
       
  );
}
  


export default App;
