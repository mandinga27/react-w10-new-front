
export const index = {
            FORMULARIO_TAREA :'TAREA_TAREA',
             OBTENER_PROYECTOS : 'OBTENER_PROYECTOS',
             AGREGAR_PROYECTO : 'AGREGAR_PROYECTO',
             VALIDAR_FORMULARIO : 'VALIDAR_FORMULARIO',
            //mostrara el nombre de la tarea al pincharla
            //en el menu izquierdo lo mostrara al centro de la pantalla
             PROYECTO_ACTUAL : 'PROYECTO_ACTUAL',
             ELIMINAR_PROYECTO : 'ELIMINAR_PROYECTO',
             PROYECTO_ERROR : 'PROYECTO_ERROR',

             TAREAS_PROYECTO : 'TAREAS_PROYECTO',
             AGREGAR_TAREA : 'AGREGAR_TAREA',
             VALIDAR_TAREA : 'VALIDAR_TAREA',
             ELIMINAR_TAREA : 'ELIMINAR_TAREA',
             ESTADO_TAREA : 'ESTADO_TAREA',
             TAREA_ACTUAL : 'TAREA_ACTUAL',
             ACTUALIZAR_TAREA : 'ACTUALIZAR_TAREA',
             LIMPIAR_TAREA : 'LIMPIAR_TAREA',

            //MOSTRAR ALERTA mostrara en pantalla una alerta
              MOSTRAR_ALERTA : 'MOSTRAR_ALERTA',
              OCULTAR_ALERTA : 'OCULTAR_ALERTA', 

              REGISTRO_EXITOSO : 'REGISTRO_EXITOSO', 
              REGISTRO_ERROR : 'REGISTRO_ERROR', 

              OBTENER_USUARIO : 'OBTENER_USUARIO', 

              LOGIN_EXITOSO : 'LOGIN_EXITOSO',
              LOGIN_ERROR : 'LOGIN_ERROR',
              CERRAR_SESION : 'CERRAR_SESION',

              ELIMINAR_USUARIO : 'ELIMINAR_USUARIO',
              AGREGAR_USUARIO : 'AGREGAR_USUARIO',
              OBTENER_USUARIOS : 'OBTENER_USUARIOS',
              FORMULARIO_USUARIOS : 'FORMULARIO_USUARIOS',
              USUARIO_ACTUAL : 'USUARIO_ACTUAL',
              ACTUALIZAR_USUARIO : 'ACTUALIZAR_USUARIO',
}


