import React, { useReducer } from 'react';
import AuthContext from './authContext';
import AuthReducer from './authReducer';

import clienteAxios from '../../config/axios';
import tokenAuth from '../../config/tokenAuth';

import { index } from '../../types'; 



const AuthState = props => {
    //state inicial
    const initialState = {
        //este token se almacenará en localstorage
        token: localStorage.getItem('token'),
        autenticado: null,
        usuario: null,
        mensaje: null,
        cargando: true
    }
    
    const [ state, dispatch ] = useReducer(AuthReducer, initialState);

    //creando las funciones
    
    //Registrar usuario
    const registrarUsuario = async datos => {
        
        try {
            const respuesta = await clienteAxios.post('/api/usuarios', datos);
            console.log(respuesta.data);

            dispatch({
                type: index.REGISTRO_EXITOSO,
                payload: respuesta.data
            });
            //cuando se registra mandamos a llamar a la funcionusuarioAutenticado();
            //Obtener el usuario una vez que tenga un registro exitoso
            usuarioAutenticado();

        } catch (error) {
            //console.log(error.response.data.msg);
            //en alertaState toma un msg y una categoria
            const alerta = {
                msg: error.response.data.msg,
                categoria: 'alerta-error'
            }

            dispatch({
                type: index.REGISTRO_ERROR,
                payload: alerta
            })
        }
    }

    //Retorna e usuario autenticado
    const usuarioAutenticado = async () => {
        //aca vamos a leer el token guardado en localStorage
        const token = localStorage.getItem('token');
        if(token) {
            //TODO: funcion para enviar el token por headers
            tokenAuth(token);
        }

        try {
            const respuesta = await clienteAxios.get('/api/auth');
            //console.log(respuesta);
            dispatch({
                type: index.OBTENER_USUARIO,
                payload: respuesta.data.usuario
            });

        } catch (error) {
            console.log(error.response);
            dispatch({
                type: index.LOGIN_ERROR
            })
        }
    }

    //Cuando el usuario inicia sesión
    export const iniciarSesion = async datos => {
        try {
            const respuesta = await clienteAxios.post('/api/auth', datos);
            console.log(respuesta);

            dispatch({
                //payload respuesta.data pasará el token si el login es exitoso
                type: index.LOGIN_EXITOSO,
                payload: respuesta.data
            });

            //Obtener el usuario
            usuarioAutenticado();

        } catch (error) {
            console.log(error.response.data.msg);
            const alerta = {
                msg: error.response.data.msg,
                categoria: 'alerta-error'
            }

            dispatch({
                type: index.LOGIN_ERROR,
                payload: alerta
            })
        }
    }

    //Cierra la sesión del usuario
    const cerrarSesion = () => {
        dispatch({
            type: index.CERRAR_SESION
        })
    }

    return (
        <AuthContext.Provider
            value={{
                token: state.token,
                autenticado: state.autenticado,
                usuario: state.usuario,
                mensaje: state.mensaje,
                cargando: state.cargando,
                registrarUsuario,
                iniciarSesion,
                usuarioAutenticado,
                cerrarSesion
            }}
        >{props.children}

        </AuthContext.Provider>
    )
}

export default AuthState;
