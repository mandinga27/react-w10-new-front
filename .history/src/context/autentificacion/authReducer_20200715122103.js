import { index } from '../../types';


export default (state, action) => {

    switch (action.type) {
        case index.REGISTRO_EXITOSO:
        case index.LOGIN_EXITOSO:
            localStorage.setItem('token', action.payload.token);
            return {
                ...state,
                autenticado: true,
                mensaje: null,
                cargando: false
            }
        case index.OBTENER_USUARIO:
            return {
                ...state,
                autenticado: true,
                usuario: action.payload,
                cargando: false
            }
        //CERRAR SESION, LOGIN ERROR Y REGISTRO ERROR HACEN CASI LO MISMO POR ESO ESTAN JUNTOS
        case index.CERRAR_SESION:
        case index.LOGIN_ERROR:
        case index.REGISTRO_ERROR:
            //en caso de error removemos el token creado
            localStorage.removeItem('token');
            return {
                ...state,
                token: null,
                usuario: null,
                autenticado: null,
                mensaje: action.payload,
                cargando: false
            }                    
        default:
            return state;
    }
}