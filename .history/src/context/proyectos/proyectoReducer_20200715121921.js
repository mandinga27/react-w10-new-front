import { index } from '../../types';


export default (state, action) => {
    switch(action.type) {
        case index.FORMULARIO_TAREA:
            return {
                //aca toma una copia del state con todo lo que tenga y lo cambia a true el formulario
                ...state,
                formulario : true
            }
        case index.OBTENER_PROYECTOS:
            return {
                ...state,
                proyectos: action.payload
            }
        case index.AGREGAR_PROYECTO:
            return {
                //se copia el state actual
                ...state,
                //se toma el state con los proyectos y se agrega el nuevo con action.payload
                proyectos: [...state.proyectos, action.payload],
                //despues que se agrega el proyecto quedara como false para que se reinicie
                formulario: false,
                //una vez que se valida y se agrega la nueva tarea, reseteamos el errorformulario 
                //para que deje de aparecer en pantalla
                errorformulario: false
                
            }
        case index.VALIDAR_FORMULARIO:
            return {
                ...state,
                errorformulario: true
            }
        case index.PROYECTO_ACTUAL:
            return {
                ...state,
                //vamos a poner un filtro, 
                //hace una iteración con cada uno de ellos comparandolos, extrae el que se elige como proyecto actual
                proyecto: state.proyectos.filter(proyecto => proyecto._id === action.payload )
            }
        case index.ELIMINAR_PROYECTO:
            return {
                //aca recorre los proyectos pero deja afuera el que queremos eliminar !==..
                proyectos: state.proyectos.filter(proyecto => proyecto._id !== 
                    action.payload ),
                    proyecto: null
            }
        case index.PROYECTO_ERROR:
            return {
                ...state,
                mensaje: action.payload
            }      
        default:
            return state;
    }
}